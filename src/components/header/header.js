import React from 'react';
import { BrowserRouter as Router, Route, Switch, NavLink } from 'react-router-dom';

// components
import Home from '../../containers/home/Home';
import Thanks from '../../containers/thanks/Thanks';

// icons
import TwitterIcon from '../../assets/images/twitter.png';
import TwitterSave from '../../assets/images/save.png';

// style
import styles from './Header.module.scss';

const Header = () => {
  return (
    <Router>
      <div className={styles.navigationContainer}>
        <div className={styles.header}>
          <img src={TwitterIcon} alt="twitter" />
          <img src={TwitterSave} alt="save" />
        </div>
        <ul>
          <li>
            <NavLink to="/">Home</NavLink>
          </li>
          <li>
            <NavLink to="/thanks">Thanks</NavLink>
          </li>
        </ul>
      </div>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/thanks" component={Thanks} />
      </Switch>
    </Router>
  );
};

export default Header;
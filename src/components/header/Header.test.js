import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

let component;
beforeEach(() => {
  component = shallow(<Header />);
});

it('has two img elements', () => {
  expect(component.find('img').length).toEqual(2);
});

it('has one Switch element', () => {
  expect(component.find('Switch').length).toEqual(1);
});
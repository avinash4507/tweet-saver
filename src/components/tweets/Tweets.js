import React from 'react';
import PropTypes from 'prop-types';

// component
import Tweet from './tweet/Tweet';

// icons
import DeleteIcon from '../../assets/images/delete.png';

// style
import styles from './Tweets.module.scss';

const Tweets = (props) => {
  let { index, tweet, createdDate, isDisplayTweets, handleItemDelete } = props;
  return (
    <div draggable
      className={styles.tweet}
      onDragStart={(event) => event.dataTransfer.setData("text/plain", index)}>
      <Tweet name={tweet.user.name}
        image={tweet.user.profileImageURL}
        twitterHandle={`@${tweet.user.screenName}`}
        date={createdDate}
        tweetText={tweet.text}>
      </Tweet>
      {isDisplayTweets ?
        null :
        <button className={styles.deleteButton} onClick={() => handleItemDelete(index)}>
          <img className={styles.deleteIcon} src={DeleteIcon} alt="delete" />
        </button>
      }
    </div>
  );
};

Tweets.propTypes = {
  index: PropTypes.number.isRequired,
  tweet: PropTypes.shape({
    user: PropTypes.shape({
      name: PropTypes.string.isRequired,
      profileImageURL: PropTypes.string.isRequired,
      screenName: PropTypes.string.isRequired,
    }),
    text: PropTypes.string.isRequired,
  }),
  createdDate: PropTypes.string.isRequired,
  isDisplayTweets: PropTypes.bool.isRequired,
  handleItemDelete: PropTypes.func.isRequired,
};

export default Tweets;
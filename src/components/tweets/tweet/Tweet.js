import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

// style
import styles from './Tweet.module.scss';

const Tweet = (props) => {
  let { image, name, twitterHandle, date, tweetText } = props;
  return (
    <Fragment>
      <img className={styles.image} src={image} alt="user profile" />
      <div className={styles.content}>
        <div className={styles.row}>
          <div className={styles.rowUserNameHandle}>
            <span className={styles.userName}>{name}</span>
            <span className={styles.twitterHandle}>{twitterHandle}</span>
          </div>
          <span className={styles.date}>{date}</span>
        </div>
        <span className={styles.tweetText}>{tweetText}</span>
      </div>
    </Fragment>
  );
};

Tweet.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  twitterHandle: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  tweetText: PropTypes.string.isRequired
};

export default Tweet;
import React from 'react';
import { shallow } from 'enzyme';
import SearchBar from './SearchBar';

let component;
const testState = { searchValue: '' };
beforeEach(() => {
  component = shallow(
    <SearchBar
      searchValue={testState.searchValue}
      handleInputChange={(e) => {
        testState[e.target.name] = e.target.value;
      }}
      handleSearch={() => { }}
    />
  );
});

it('has an input where users can type in', () => {
  component.find('input').simulate('change', { target: { name: 'searchValue', value: 'canada' } });
  expect(testState.searchValue).toEqual('canada');
});

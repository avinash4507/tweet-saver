import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// icon
import SearchImage from '../../assets/images/search.png';

// style
import styles from './SearchBar.module.scss';

const SearchBar = (props) => {
  let { searchValue, handleInputChange, handleSearch } = props;
  return (
    <Fragment>
      <input className={styles.searchInput}
        value={searchValue}
        onChange={(e) => handleInputChange(e)}
        onKeyDown={(e) => handleSearch(e)}
        type="search"
        maxLength={50}
        placeholder="Search Twitter"
        name="searchBar" id="search-bar"
      />
      <button className={styles.button} onClick={(e) => handleSearch(e)}>
        <img className={styles.image} src={SearchImage} alt="Search" />
      </button>
    </Fragment>
  );
};

SearchBar.propTypes = {
  searchValue: PropTypes.string,
  handleInputChange: PropTypes.func,
  handleSearch: PropTypes.func,
};

export default SearchBar;
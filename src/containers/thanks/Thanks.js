import React, { Component } from 'react';

// icon
import ThanksBg from '../../assets/images/thanks.jpg';

class Thanks extends Component {
  render() {
    return (
      <img className="thanksImage" src={ThanksBg} alt="thanks" />
    );
  }
}

export default Thanks;

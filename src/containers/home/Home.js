import React, { Component } from 'react';
import * as jsonp from 'jsonp';

// components
import SearchBar from '../../components/searchBar/SearchBar';

// icons
import TwitterIcon from '../../assets/images/twitter.png';
import EmptyBox from '../../assets/images/empty.png';

// style
import styles from './Home.module.scss';
import Tweets from '../../components/tweets/Tweets';

class Home extends Component {
  state = {
    tweets: [],
    savedTweets: [],
    searchValue: '',
    loading: false
  };

  months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  componentDidMount() {
    let savedTweets = localStorage.getItem('savedTweets');
    if (savedTweets) {
      this.setState({ savedTweets: JSON.parse(savedTweets) });
    }
  }

  /**
   * function to handle dropped tweet
   */
  handleDrop = (event) => {
    this.stopPropagation(event);
    let id = event.dataTransfer.getData("text");
    let givenTweetIndex = this.state.savedTweets.findIndex(tweet => this.state.tweets[id].id === tweet.id);
    if (givenTweetIndex < 0) {
      this.setState((state) => {
        return {
          savedTweets: [state.tweets[id], ...state.savedTweets],
        };
      });
      event.currentTarget.style.background = '#B9F6CA';
      localStorage.setItem('savedTweets', JSON.stringify([this.state.tweets[id], ...this.state.savedTweets]));
    } else {
      event.currentTarget.style.background = '#FF8A80';
    }
    this.removeBackgroundColor();
    event.dataTransfer.clearData();
  };

  /**
   * function to remove background color after sometime
   */
  removeBackgroundColor = () => {
    setTimeout(() => {
      let container = document.getElementById('saved-tweet-class');
      container.style.background = 'white';
    }, 500);
  };

  /**
   * function to delete saved tweet
   */
  handleItemDelete = (index) => {
    let savedTweetsCopy = JSON.parse(JSON.stringify(this.state.savedTweets));
    savedTweetsCopy.splice(index, 1);
    this.setState({ savedTweets: savedTweetsCopy });
    localStorage.setItem('savedTweets', JSON.stringify(savedTweetsCopy));
  };

  /**
   * function to get saved or searched tweets
   */
  getTweets = (tweets, isDisplayTweets) => {
    return tweets.map((tweet, index) => {
      let date = new Date(tweet.createdAt);
      let createdDate = `${date.getDate()} ${this.months[date.getMonth()]} ${date.getFullYear().toString().slice(2)}`;
      return (
        <Tweets key={tweet.idStr}
          tweet={tweet}
          index={index}
          isDisplayTweets={isDisplayTweets}
          createdDate={createdDate}
          handleItemDelete={(index) => this.handleItemDelete(index)}>
        </Tweets>
      );
    });
  };

  /**
   * function to stop event propagation
   */
  stopPropagation = (event) => {
    event.preventDefault();
    event.stopPropagation();
  };

  /**
   * Search api call
   */
  onSearch = (event) => {
    if (event.keyCode === 13 || event.type === 'click') {
      event.preventDefault();
      let searchValue = this.state.searchValue.trim();
      if (searchValue.length) {
        this.setState({ loading: true, tweets: [] });
        jsonp(`http://tweetsaver.herokuapp.com/?q=${searchValue}&count=10`,
          { timeout: 5000 }
          , (err, data) => {
            if (err) {
              this.setState({ loading: false });
              console.error(err.message);
            } else {
              this.setState({ loading: false, tweets: data.tweets });
            }
          });
      } else {
        this.setState({ tweets: [] });
      }
    }
  };

  render() {
    // dynamic classes
    let displayTweetsClass = this.state.loading || this.state.tweets.length === 0 ?
      styles.flexContainer : styles.fullHeight;
    let savedTweetsClass = this.state.savedTweets.length === 0 ?
      styles.flexContainer : styles.fullHeight;

    return (
      <div className={styles.row}>

        {/* search-bar and searched tweets container */}
        <div className={styles.column}>
          <div className={styles.upperContainer}>
            <SearchBar handleSearch={(e) => this.onSearch(e)}
              searchValue={this.state.searchValue}
              handleInputChange={(e) => this.setState({ searchValue: e.target.value })}>
            </SearchBar>
          </div>

          {/* container to show searched tweets */}
          <div className={displayTweetsClass}>
            {!this.state.loading &&
              this.state.tweets.length === 0 ?
              <img src={TwitterIcon} alt="twitter" /> :
              this.getTweets(this.state.tweets, true)
            }
            {/* show loader */}
            {this.state.loading ?
              <div className={styles.loader}></div> : null
            }
          </div>
        </div>

        {/* saved tweets container*/}
        <div className={styles.column}>
          <h2 className={styles.upperContainer}>Saved Tweets</h2>
          <div id="saved-tweet-class" className={savedTweetsClass}
            onDragStart={(event) => this.stopPropagation(event)}
            onDragOver={(event) => this.stopPropagation(event)}
            onDrop={(event) => this.handleDrop(event)}>
            {this.state.savedTweets.length > 0 ?
              this.getTweets(this.state.savedTweets, false) :
              <img src={EmptyBox} alt="delete" />
            }
          </div>
        </div>
      </div>
    );
  }
}

export default Home;

import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
import Header from './components/header/Header';

it('has header component', () => {
  const component = shallow(<App />);
  expect(component.find(Header).length).toEqual(1);
});
